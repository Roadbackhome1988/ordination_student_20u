package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {

	@Test
	public void opretPNOrdination1() {
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");	
		PN pn1 = Controller.opretPNOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 15), p1, l1, 100);

		assertNotNull(pn1);
		assertEquals("Paracetamol", pn1.getType());
		assertTrue(p1.getOrdinationer().contains(pn1));
		assertEquals(LocalDate.of(2021, 1, 15), pn1.getSlutDen());
		}
	
	@Test(expected = IllegalArgumentException.class)
	public void opretPNOrdination2() {
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");	
		PN pn1 = Controller.opretPNOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 11), p1, l1, 100);
		
		assertNull(pn1);
	}
	
	@Test
	public void ordinationPNAnvendt1() {
		LocalDate givetDen = LocalDate.of(2021, 1, 13);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");	
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		
		PN pn1 = Controller.opretPNOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 14), p1, l1, 100);
		Controller.ordinationPNAnvendt(pn1, givetDen);
		assertEquals(1,pn1.getAntalGangeGivet());	
		assertEquals(givetDen, pn1.getDageGivet().get(0));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void ordinationPNAnvendt2() {
		LocalDate givetDen = LocalDate.of(2021, 2, 20);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");	
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		
		PN pn1 = Controller.opretPNOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 14), p1, l1, 100);
		Controller.ordinationPNAnvendt(pn1, givetDen);
		assertEquals(1,pn1.getAntalGangeGivet());	
		assertEquals(givetDen, pn1.getDageGivet().get(0));
	}
	
	@Test
	public void ordinationPNAnvendt3() {
		LocalDate givetDen = LocalDate.of(2021, 1, 12);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");	
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		
		PN pn1 = Controller.opretPNOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 14), p1, l1, 100);
		Controller.ordinationPNAnvendt(pn1, givetDen);
		assertEquals(1,pn1.getAntalGangeGivet());	
		assertEquals(givetDen, pn1.getDageGivet().get(0));
	}
	
	@Test
	public void ordinationPNAnvendt4() {
		LocalDate givetDen = LocalDate.of(2021, 1, 14);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");	
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		
		PN pn1 = Controller.opretPNOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 14), p1, l1, 100);
		Controller.ordinationPNAnvendt(pn1, givetDen);
		assertEquals(1,pn1.getAntalGangeGivet());
		assertEquals(givetDen, pn1.getDageGivet().get(0));
	}
	
	@Test
	public void testOpretDagligFastOrdinationGyldigData() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		DagligFast dagligFast1 = Controller.opretDagligFastOrdination(LocalDate.of(2021, 2, 9), LocalDate.of(2021, 2, 24), p1, l1, 0, 0, 0, 0);
		//assert
		assertNotNull(dagligFast1);
		assertEquals("Paracetamol", dagligFast1.getType());
		assertTrue(p1.getOrdinationer().contains(dagligFast1));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdinationUgyldigData() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",96.8);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		DagligFast dagligFast2 = Controller.opretDagligFastOrdination(LocalDate.of(2021, 2, 9), LocalDate.of(2021, 2, 8), p1, l1, 0, 0, 0, 0);
		//assert
		assertNull(dagligFast2);
	}
	
	@Test
	public void testAnbefaletDosisPrDoegn24() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",24);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		double dose = Controller.anbefaletDosisPrDoegn(p1, l1);
		//assert
		assertEquals(24, dose , 0.001);
	}
	
	@Test
	public void testAnbefaletDosisPrDoegn25() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",25);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		double dose = Controller.anbefaletDosisPrDoegn(p1, l1);
		//assert
		assertEquals(37.5, dose , 0.001);
	}
	
	@Test
	public void testAnbefaletDosisPrDoegn120() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",120);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		double dose = Controller.anbefaletDosisPrDoegn(p1, l1);
		//assert
		assertEquals(180, dose , 0.001);
	}
	
	@Test
	public void testAnbefaletDosisPrDoegn121() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",121);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		double dose = Controller.anbefaletDosisPrDoegn(p1, l1);
		//assert
		assertEquals(242, dose , 0.001);
	}
	
	@Test
	public void testAnbefaletDosisPrDoegnKommatal() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",119.5);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		double dose = Controller.anbefaletDosisPrDoegn(p1, l1);
		//assert
		assertEquals(179.25, dose , 0.001);
	}
	
	@Test
	public void testAnbefaletDosisPrDoegn1() {
		//arrange
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",1);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		//act
		double dose = Controller.anbefaletDosisPrDoegn(p1, l1);
		//assert
		assertEquals(1, dose , 0.001);
	}
	@Test
	//Test hvis vægt er størst
	public void antalOrdinationerPrVægtPrLægemiddel1() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
		double vægtStart = 100;
		double vægtSlut = 80;
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",86.5);
		Patient p2 = Controller.opretPatient("200270-2525", "Karsten Karstensen",56.7);
		Patient p3 = Controller.opretPatient("200270-2525", "Karsten Karstensen",24.6);
		Laegemiddel l1 = Controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		DagligSkaev o3 = new DagligSkaev(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1);
		DagligSkaev o = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l1, kl, an);
		p2.addOrdination(o3);
		p1.addOrdination(o3);
		int antal = Controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut,l1);
		assertEquals(0,antal);
		
	}
	@Test
	//Test normalforløb
	public void antalOrdinationerPrVægtPrLægemiddel2() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
		double vægtStart = 20;
		double vægtSlut = 90;
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",23);
		Patient p2 = Controller.opretPatient("200270-2525", "Karsten Karstensen",56.5);
		Patient p3 = Controller.opretPatient("200270-2525", "Karsten Karstensen",89.9);
		Laegemiddel l1 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");
		Laegemiddel l2 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");		
		DagligSkaev o3 = new DagligSkaev(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1);
		DagligSkaev o = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l1, kl, an);
		p1.addOrdination(o3);
		int antal = Controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut,l1);
		assertEquals(2,antal);		
	}
	
	@Test
	//Test normalforløb
	public void antalOrdinationerPrVægtPrLægemiddel3() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
		double vægtStart = 90;
		double vægtSlut = 140;
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",23);
		Patient p2 = Controller.opretPatient("200270-2525", "Karsten Karstensen",56.5);
		Patient p3 = Controller.opretPatient("200270-2525", "Karsten Karstensen",89.9);
		Laegemiddel l1 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");
		Laegemiddel l2 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");		
		DagligSkaev o3 = new DagligSkaev(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1);
		DagligSkaev o = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l1, kl, an);
		DagligSkaev o1 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l2, kl, an);
		p1.addOrdination(o3);
		
		int antal = Controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut,l1);
		assertEquals(0,antal);		
	}
	
	
	
	@Test
	//Test start er negativ
	public void antalOrdinationerPrVægtPrLægemiddel4() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
		double vægtStart = -5;
		double vægtSlut = 60;
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",23);
		Patient p2 = Controller.opretPatient("200270-2525", "Karsten Karstensen",56.5);
		Patient p3 = Controller.opretPatient("200270-2525", "Karsten Karstensen",89.9);
		Laegemiddel l1 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");
		Laegemiddel l2 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");		
		DagligSkaev o3 = new DagligSkaev(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1);
		DagligSkaev o = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p2, l1, kl, an);
		DagligSkaev o1 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l2, kl, an);
		p1.addOrdination(o3);		
		int antal = Controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut,l1);
		assertEquals(2,antal);		
	}
	@Test
	//Test vægtslut er negativ
	public void antalOrdinationerPrVægtPrLægemiddel5() {
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
		double vægtStart = 90;
		double vægtSlut = -5;
		Patient p1 = Controller.opretPatient("200270-2525", "Karsten Karstensen",23);
		Patient p2 = Controller.opretPatient("200270-2525", "Karsten Karstensen",56.5);
		Patient p3 = Controller.opretPatient("200270-2525", "Karsten Karstensen",89.9);
		Laegemiddel l1 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");
		Laegemiddel l2 = Controller.opretLaegemiddel("Acetylsalicylsyre", 1, 1.5, 2, "Ml");		
		DagligSkaev o3 = new DagligSkaev(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1);
		DagligSkaev o = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l1, kl, an);
		DagligSkaev o1 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), p3, l2, kl, an);
		p1.addOrdination(o3);		
		int antal = Controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut,l1);
		assertEquals(0,antal);		
	}
	
	
	
	@Test
	public void testOpretDagligSkaevOrdination1() {
		//tester om enheder og klokkeSlet ens
		//arrange
		Patient p1 = new Patient("151515-1515", "Erik Dyson", 24.0);
		Laegemiddel l1 = new Laegemiddel("Panodil", 2, 3, 4, "stk");
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };
		//act
        DagligSkaev d1 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 5), p1, l1,
	         kl, an);
		//assert
        assertEquals(4, kl.length);
        assertEquals(4, an.length);
        assertNotNull(d1);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination2() {
		//tester om enheder er mindre klokkeSlet
		//arrange
		Patient p2 = new Patient("151515-1515", "Erik Dyson", 24.0);
		Laegemiddel l2 = new Laegemiddel("Panodil", 2, 3, 4, "stk");
		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45), LocalTime.of(20, 15) };
        double[] an = { 0.5, 1, 2.5 };
		//act
        DagligSkaev d2 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 5), p2, l2,
	         kl, an);
		//assert
        assertEquals(5, kl.length);
        assertEquals(3, an.length);
        assertNull(d2);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination3() {
		//tester om enheder er større end klokkeSlet
		//arrange
		Patient p3 = new Patient("151515-1515", "Erik Dyson", 24.0);
		Laegemiddel l3 = new Laegemiddel("Panodil", 2, 3, 4, "stk");
		LocalTime[] kl = { LocalTime.of(12, 0) };
        double[] an = { 0.5, 1, 2.5, 3, 1, 7 };
		//act
        DagligSkaev d2 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 5), p3, l3,
	         kl, an);
		//assert
        assertEquals(1, kl.length);
        assertEquals(6, an.length);
        assertNull(d2);
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination4() {
	//Tester om	
	Patient p4 = new Patient("151515-1515", "Erik Dyson", 24.0);
	Laegemiddel l4 = new Laegemiddel("Panodil", 2, 3, 4, "stk");
	LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
            LocalTime.of(16, 0), LocalTime.of(18, 45) };
    double[] an = { 0.5, 1, 2.5, 3 };
	//act
    DagligSkaev d4 = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 4, 1), LocalDate.of(2021, 3, 5), p4, l4,
         kl, an);
	//assert
    assertNull(d4);
}
	
	
	
	
	
	
	
	
	
}
