package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;

public class TestDagligSkaev {

	//Metode: opretDosis
	@Test
	public void testOpretDosis1() {
		//act
		Dosis Antal3 = new Dosis(LocalTime.of(10, 30), 3);
		//assert
		assertEquals(LocalTime.of(10, 30), Antal3.getTid());
		assertEquals(3, Antal3.getAntal(), 0.01);
	}
	
	@Test
	public void testOpretDosis2() {
		//act
		Dosis Antal0 = new Dosis(LocalTime.of(10, 30), 0);
		//assert
		assertEquals(LocalTime.of(10, 30), Antal0.getTid());
		assertEquals(0, Antal0.getAntal(), 0.01);
	}
	
	@Test
	public void testOpretDosis3() {
		//act
		Dosis Antal1 = new Dosis(LocalTime.of(10, 30), 1);
		//assert
		assertEquals(LocalTime.of(10, 30), Antal1.getTid());
		assertEquals(1, Antal1.getAntal(), 0.01);
	}
	
	@Test
	public void testOpretDosis4() {
		//act
		Dosis Antal5000 = new Dosis(LocalTime.of(10, 30), 5000);
		//assert
		assertEquals(LocalTime.of(10, 30), Antal5000.getTid());
		assertEquals(5000, Antal5000.getAntal(), 0.01);
	}
	
	//Metode: Samlet Dosis
	
	@Test
	public void testSamletDosis1() {
		//arrange
		Laegemiddel l = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		DagligSkaev daglig1 = new DagligSkaev(LocalDate.of(2021, 2, 10), LocalDate.of(2021, 2, 19), l);
		daglig1.opretDosis(LocalTime.of(10, 30), 0);
		daglig1.opretDosis(LocalTime.of(10, 30), 1);
		daglig1.opretDosis(LocalTime.of(10, 30), 2);
		//act
		double samlet = daglig1.samletDosis();
		//assert
		assertEquals(30, samlet, 0.01);
	}
	
	@Test
	public void testSamletDosis2() {
		//arrange
		Laegemiddel l = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		DagligSkaev daglig1 = new DagligSkaev(LocalDate.of(2021, 2, 10), LocalDate.of(2021, 2, 10), l);
		daglig1.opretDosis(LocalTime.of(10, 30), 2);
		daglig1.opretDosis(LocalTime.of(10, 30), 3);
		//act
		double samlet = daglig1.samletDosis();
		//assert
		assertEquals(5, samlet, 0.01);
	}
	
	@Test
	public void testSamletDosis3() {
		//arrange
		Laegemiddel l = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		DagligSkaev daglig1 = new DagligSkaev(LocalDate.of(2021, 2, 10), LocalDate.of(2021, 2, 17), l);
		daglig1.opretDosis(LocalTime.of(10, 30), 3);
		daglig1.opretDosis(LocalTime.of(10, 30), 2);
		daglig1.opretDosis(LocalTime.of(10, 30), 2);
		daglig1.opretDosis(LocalTime.of(10, 30), 0);
		daglig1.opretDosis(LocalTime.of(10, 30), 1);
		//act
		double samlet = daglig1.samletDosis();
		//assert
		assertEquals(64, samlet, 0.01);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSamletDosis4() {
		//arrange
		Laegemiddel l = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		DagligSkaev daglig1 = new DagligSkaev(LocalDate.of(2021, 2, 10), LocalDate.of(2021, 2, 12), l);
		daglig1.opretDosis(LocalTime.of(10, 30), -1);
		daglig1.opretDosis(LocalTime.of(10, 30), 5);
		//act
		daglig1.samletDosis();
	}
	
	//Metode: DoegnDosis
	
	@Test
	public void testDoegnDosis() {
		//arrange
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		DagligSkaev dagligSkeav = new DagligSkaev(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 11), l);
		dagligSkeav.opretDosis(LocalTime.of(8, 0), 1);
		dagligSkeav.opretDosis(LocalTime.of(12, 0), 4);
		dagligSkeav.opretDosis(LocalTime.of(18, 0), 0);
		dagligSkeav.opretDosis(LocalTime.of(23, 0), 1);
		//act
		double doegnDosis = dagligSkeav.doegnDosis();
		//assert
		assertEquals(6, doegnDosis, 0.001);
	}
}
