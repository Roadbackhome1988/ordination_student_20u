package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;

public class OrdinationTest {

	@Test
	public void testAntalDageDagligFast() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 5), l);
		
		assertEquals(4, dagligFast.antalDage());
	}
	
	@Test
	public void testAntalDageDagligSkaev() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		DagligSkaev dagligSkaev = new DagligSkaev(LocalDate.of(2021, 2, 28), LocalDate.of(2021, 3, 10), l);
		
		assertEquals(11, dagligSkaev.antalDage());
	}
	
	@Test
	public void testAntalDagePN() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN(LocalDate.of(2021, 2, 2), LocalDate.of(2021, 5, 2), l, 0);
		assertEquals(90, pn.antalDage());
	}
	
	@Test
	public void testAntalDageSlutFoerStart() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 1), l);
		
		assertEquals(0, dagligFast.antalDage());
	}
}
