package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligFastTest {
	
	@Test
	public void testSamletDosis_DoseHverGang() {
		//arrange 
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Dosis d1 = new Dosis(LocalTime.of(8, 0), 5);
		Dosis d2 = new Dosis(LocalTime.of(12, 0), 3);
		Dosis d3 = new Dosis(LocalTime.of(18, 0), 10);
		Dosis d4 = new Dosis(LocalTime.of(23, 0), 6);
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 6), l);
		dagligFast.addDosis(d1, 0);
		dagligFast.addDosis(d2, 1);
		dagligFast.addDosis(d3, 2);
		dagligFast.addDosis(d4, 3);
		//act
		double samlet = dagligFast.samletDosis();
		//assert
		assertEquals(120, samlet, 0.001);
	}
	
	@Test
	public void testSamletDosis_NulDoserEnGang() {
		//arrange 
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Dosis d1 = new Dosis(LocalTime.of(8, 0), 1);
		Dosis d2 = new Dosis(LocalTime.of(12, 0), 6);
		Dosis d3 = new Dosis(LocalTime.of(18, 0), 0);
		Dosis d4 = new Dosis(LocalTime.of(23, 0), 2);
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 15), l);
		dagligFast.addDosis(d1, 0);
		dagligFast.addDosis(d2, 1);
		dagligFast.addDosis(d3, 2);
		dagligFast.addDosis(d4, 3);
		//act
		double samlet = dagligFast.samletDosis();
		//assert
		assertEquals(126, samlet, 0.001);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testSamletDosis_MinusDose() {
		//arrange 
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Dosis d1 = new Dosis(LocalTime.of(8, 0), -1);
		Dosis d2 = new Dosis(LocalTime.of(12, 0), 5);
		Dosis d3 = new Dosis(LocalTime.of(18, 0), 1);
		Dosis d4 = new Dosis(LocalTime.of(23, 0), 0);
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 15), l);
		dagligFast.addDosis(d1, 0);
		dagligFast.addDosis(d2, 1);
		dagligFast.addDosis(d3, 2);
		dagligFast.addDosis(d4, 3);
		//act
		dagligFast.samletDosis();
	}
	
	@Test
	public void testSamletDosis_SkiftAfMåned() {
		//arrange 
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Dosis d1 = new Dosis(LocalTime.of(8, 0), 1);
		Dosis d2 = new Dosis(LocalTime.of(12, 0), 6);
		Dosis d3 = new Dosis(LocalTime.of(18, 0), 0);
		Dosis d4 = new Dosis(LocalTime.of(23, 0), 2);
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 28),LocalDate.of(2021, 3, 5), l);
		dagligFast.addDosis(d1, 0);
		dagligFast.addDosis(d2, 1);
		dagligFast.addDosis(d3, 2);
		dagligFast.addDosis(d4, 3);
		//act
		double samlet = dagligFast.samletDosis();
		//assert
		assertEquals(54, samlet, 0.001);
	}
	
	@Test
	public void testDoegnDosis() {
		//arrange
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Dosis d1 = new Dosis(LocalTime.of(8, 0), 5);
		Dosis d2 = new Dosis(LocalTime.of(12, 0), 3);
		Dosis d3 = new Dosis(LocalTime.of(18, 0), 10);
		Dosis d4 = new Dosis(LocalTime.of(23, 0), 6);
		DagligFast dagligFast = new DagligFast(LocalDate.of(2021, 2, 2),LocalDate.of(2021, 2, 6), l);
		dagligFast.addDosis(d1, 0);
		dagligFast.addDosis(d2, 1);
		dagligFast.addDosis(d3, 2);
		dagligFast.addDosis(d4, 3);
		//act
		double doegnDosis = dagligFast.doegnDosis();
		//assert
		assertEquals(24, doegnDosis, 0.001);
	}
}
