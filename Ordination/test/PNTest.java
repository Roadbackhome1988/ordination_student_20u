package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {
	private Laegemiddel l1 = new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
	
	@Test
	public void getAntalEnheder() {
		PN pn1 = new PN(LocalDate.of(2021, 3, 1), LocalDate.of(2021, 3, 10), l1, 40 );
		assertEquals(40, pn1.getAntalEnheder(), 0);
	}
	
	@Test
	public void givDosis1() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 40 );
		assertTrue(pn1.givDosis(LocalDate.of(2021, 1, 15)));
	}

	@Test
	public void givDosis2() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 40 );
		assertFalse(pn1.givDosis(LocalDate.of(2021, 1, 30)));
	}
	
	@Test
	public void givDosis3() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 40 );
		assertTrue(pn1.givDosis(LocalDate.of(2021, 1, 20)));
	}
	
	@Test
	public void givDosis4() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 40 );
		assertTrue(pn1.givDosis(LocalDate.of(2021, 1, 12)));
	}
	
	@Test
	public void getAntalGangeGivet1() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 40 );
		//Adder 7 doser til PN
		pn1.givDosis(LocalDate.of(2021, 1, 12));
		pn1.givDosis(LocalDate.of(2021, 1, 13));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		assertEquals(7, pn1.getAntalGangeGivet());
	}
	
	@Test
	public void getAntalGangeGivet2() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 40 );
		
		assertEquals(0, pn1.getAntalGangeGivet());
	}
	
	@Test
	//Antal sat til 50, antalGangeGivet = 5
	public void doegnDosis1() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 50 );
		pn1.givDosis(LocalDate.of(2021, 1, 12));
		pn1.givDosis(LocalDate.of(2021, 1, 13));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		
		assertEquals(27.777,pn1.doegnDosis(), 0.01);
	}
	
	@Test
	//Antal sat til 27, antalGangeGivet = 5
	public void doegnDosis2() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 27 );
		pn1.givDosis(LocalDate.of(2021, 1, 12));
		pn1.givDosis(LocalDate.of(2021, 1, 13));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));		
		assertEquals(15,pn1.doegnDosis(), 0.01);
	}
	
	@Test
	//Antal sat til 27, antalGangeGivet = 1
	public void doegnDosis3() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 27 );
		pn1.givDosis(LocalDate.of(2021, 1, 12));
		
		assertEquals(3,pn1.doegnDosis(), 0.01);
	}
	
	@Test
	//Antal sat til 27, antalGangeGivet = 0
	public void doegnDosis4() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 27 );
		assertEquals(0,pn1.doegnDosis(), 0.01);
	}
	
	@Test
	public void getType() {
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 27 );
		assertEquals("Paracetamol", pn1.getType());
	}
	
	@Test
	public void testSamletDosis1(){
		PN pn = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 27 );
		pn.givDosis(LocalDate.of(2021, 1, 12));
		pn.givDosis(LocalDate.of(2021, 1, 13));
		pn.givDosis(LocalDate.of(2021, 1, 14));
		pn.givDosis(LocalDate.of(2021, 1, 14));
		pn.givDosis(LocalDate.of(2021, 1, 14));
		double samlet = pn.samletDosis();
		assertEquals(135, samlet, 0.001);
	}
	
	public void testSamletDosis2(){
		PN pn1 = new PN(LocalDate.of(2021, 1, 12), LocalDate.of(2021, 1, 20), l1, 27 );
		pn1.givDosis(LocalDate.of(2021, 1, 12));
		pn1.givDosis(LocalDate.of(2021, 1, 13));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		pn1.givDosis(LocalDate.of(2021, 1, 14));
		double samlet = pn1.samletDosis();
		assertEquals(249.993, samlet, 0.001);
	}
}
