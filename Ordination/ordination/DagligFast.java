package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination{
	
	private Dosis[] doser = new Dosis[4];
	
	public DagligFast(LocalDate startDato, LocalDate slutDato, Laegemiddel laegemiddel) {
		super(startDato, slutDato, laegemiddel);
	}
	
	public Dosis[] getDoser() {
		return doser;
	}
	
	public void addDosis(Dosis dosis, int i) {
		doser[i] = dosis;
	}
	
	@Override
	public double samletDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			if(d.getAntal() >= 0) {
			sum += d.getAntal();
			}
			else {
				throw new IllegalArgumentException("Antallet af doser kan ikke være under 0");
			}
		}
		return sum * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = samletDosis() / antalDage();
		return sum;
	}

	@Override
	public String getType() {
		return getLaegemiddel().getNavn();
	}
	
}
