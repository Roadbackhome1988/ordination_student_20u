package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;


public class DagligSkaev extends Ordination {
	
	private LocalTime tid;
	private double antal;
	
	private ArrayList<Dosis> doser = new ArrayList<>();
	
	
	public DagligSkaev(LocalDate startDato, LocalDate slutDato, Laegemiddel laegemiddel) {
		super(startDato, slutDato, laegemiddel);
	}


    public void opretDosis(LocalTime tid, double antal) {
       Dosis dosis = new Dosis(tid, antal);
       doser.add(dosis);
    }
    
    public ArrayList<Dosis> getDoser() {
		return doser;
	}


	@Override
	public double samletDosis() {
		double sum = 0;
		for(Dosis d: doser) {
			if(d.getAntal() >= 0) {
			sum += d.getAntal();
		}
			else {
			throw new IllegalArgumentException("En dosis må ikke være mindre end 0");	
			}
	}
		return sum * antalDage();
}


	@Override
	public double doegnDosis() {
		double sum = 0;
		sum = samletDosis() / antalDage();
		return sum;
	}



	@Override
	public String getType() {
		String type = super.getLaegemiddel().getNavn();
		return type;
	}
	
	public LocalTime getTid() {
		return tid;
	}
	
	public double getAntal() {
		return antal;
	}
}
