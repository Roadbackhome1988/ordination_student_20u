package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

    private double antalEnheder;
    private ArrayList<LocalDate> dage = new ArrayList<>();
   
    public PN(LocalDate startDate, LocalDate slutDate, Laegemiddel laegemiddel, double antalEnheder) {
    	super(startDate, slutDate, laegemiddel);
    	this.antalEnheder = antalEnheder;
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }   
    
    /**
     * Registrer dagen givesDen, hvor der er givet en dosis af PN.
     * Returner true, hvis givesDen er inden for ordinationens gyldighedsperiode.
     * Returner false ellers, og datoen givesDen ignoreres.
     */
    public boolean givDosis(LocalDate givesDen) {
    	if(givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
    		dage.add(givesDen);
    		return true;    		
    	}else if(givesDen.equals(getStartDen()) || givesDen.equals(getSlutDen())){
    		dage.add(givesDen);
    		return true;
    	}else {
    		return false;
    	}
    }

    /**
     * Returner antal gange ordinationen er anvendt.
     */
    public int getAntalGangeGivet() {
    	if(dage.isEmpty()) {
    		return 0;
    	}else {
    		return dage.size();
    	}
    }
    
    public ArrayList<LocalDate> getDageGivet(){
    	return new ArrayList<>(dage);
    }
    
	@Override
	public double samletDosis() {
		double samlet = doegnDosis() * antalDage();
		return samlet;
	}

	@Override
	public double doegnDosis() {
		double antal = getAntalGangeGivet() * getAntalEnheder() / antalDage();
		return antal;
	}

	@Override
	public String getType() {
		return this.getLaegemiddel().getNavn();
	}
	
}
