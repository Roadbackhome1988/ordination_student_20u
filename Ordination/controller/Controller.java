package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public abstract class Controller {
    private static Storage storage = Storage.getInstance();

    /**
     * Opret en DagligFast ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
     * ordinationen oprettes ikke.
     */
    public static PN opretPNOrdination(LocalDate startDen, LocalDate slutDen,
            Patient patient, Laegemiddel laegemiddel, double antal) {
    	if(slutDen.isBefore(startDen)) {
    		throw new IllegalArgumentException("Slutdato må ikke være før startdato");
    	}else {
    		PN p = new PN(startDen, slutDen, laegemiddel, antal);
    	patient.addOrdination(p);
    	return p;
    	}
    }

    /**
     * Opret en DagligFast ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
     * ordinationen oprettes ikke.
     */
    public static DagligFast opretDagligFastOrdination(LocalDate startDen,
            LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
            double morgenAntal, double middagAntal, double aftenAntal,
            double natAntal) {
    	DagligFast dagligFast = new DagligFast(startDen, slutDen, laegemiddel);
    	
       if(startDen.isAfter(slutDen)) {
    	   	throw new IllegalArgumentException("Startdato kan ikke være efter slutdato");
       }
       else {
    	   patient.addOrdination(dagligFast);
    	   Dosis doseMorgen = new Dosis(LocalTime.of(8, 0), morgenAntal);
    	   dagligFast.addDosis(doseMorgen, 0);
    	   Dosis doseMiddag = new Dosis(LocalTime.of(12, 0), middagAntal);
    	   dagligFast.addDosis(doseMiddag, 1);
    	   Dosis doseAften = new Dosis(LocalTime.of(18, 0), aftenAntal);
    	   dagligFast.addDosis(doseAften, 2);
    	   Dosis doseNat = new Dosis(LocalTime.of(23, 0), natAntal);
    	   dagligFast.addDosis(doseNat, 3);
       }
        return dagligFast;
    }

    /**
     * Opret en DagligSkæv ordination.
     * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
     * ordinationen oprettes ikke.
     * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige
     * kastes en IllegalArgumentException.
     */
    public static DagligSkaev opretDagligSkaevOrdination(LocalDate startDen,
            LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
            LocalTime[] klokkeSlet, double[] antalEnheder) {
    	DagligSkaev dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel);
    	int optælKlok = 0;
    	int optælEnheder = 0;
    	for (LocalTime s : klokkeSlet) {
			if(s != null) {
				optælKlok++;
			}
		}
    	for (double e : antalEnheder) {
			if(e != 0) {
				optælEnheder++;
			}
		}
    	for(int i = 0;i<klokkeSlet.length;i++) {
    		for(int j = i;j<=i;j++) {
    			dagligSkaev.opretDosis(klokkeSlet[i], antalEnheder[j]);
    		}
    	}	
    	
    	if(optælKlok != optælEnheder) {
    		throw new IllegalArgumentException("Antallet af enheder passer ikke med antallet af klokkeslæt");
    	}
    	else if(startDen.isAfter(slutDen)) {
    		throw new IllegalArgumentException("Startdato kan ikke være efter slutdato");
    	}
    	else {
    		patient.addOrdination(dagligSkaev);
    	}
    	return dagligSkaev;
    }

    /**
     * Tilføj en dato for anvendelse af PN ordinationen.
     * Hvis datoen ikke er indenfor ordinationens gyldighedsperiode
     * kastes en IllegalArgumentException.
     */
    public static void ordinationPNAnvendt(PN ordination, LocalDate dato) {
    	if(ordination.givDosis(dato)) {
    		
    	}else {
    		throw new IllegalArgumentException("datoen er ikke indenfor ordinationens gyldighedsperiode");
    	}
    }

    /**
     * Returner den anbefalede dosis af lægemidlet for patienten.
     * (Beregningen anvender en enhedsfaktor, der er afhængig
     * af patientens vægt.)
     */
    //pre: patient.getVaegt > 0
    public static double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
        double dose = 0;
        if(patient.getVaegt() < 25) {
        	dose = laegemiddel.getEnhedPrKgPrDoegnLet() * patient.getVaegt();
        }
        else if(patient.getVaegt() > 120) {
        	dose = laegemiddel.getEnhedPrKgPrDoegnTung() * patient.getVaegt();
        }
        else {
        	dose = laegemiddel.getEnhedPrKgPrDoegnNormal() * patient.getVaegt();
        }
        return dose;
    }

    /**
     * Returner antal ordinationer af lægemidlet
     * for patienter med vægt i intervallat vægtStart..vægtslut.
     * Pre:vægtStart > vægtSlut og begge er > 0.  
     * Lægemiddel, patient og ordination er oprettet.
     */
    public static int antalOrdinationerPrVægtPrLægemiddel(
            double vægtStart, double vægtSlut, Laegemiddel laegemiddel) {
    	int antalOrdinationer = 0;
    		for (Patient p: getAllPatienter())
    			if(p.getVaegt() >= vægtStart 
    			&& p.getVaegt() <= vægtSlut) 
    			for(Ordination o : p.getOrdinationer()) {
    				if(o.getLaegemiddel() == laegemiddel) {
    					antalOrdinationer ++;
    				}	
    			}
    		 return antalOrdinationer;
    	}

    //-----------------------------------------------------

    /**
     * Returner true, hvis slutDato <= slutDato.
     */
    private static boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
        return startDato.compareTo(slutDato) <= 0;
    }

    public static Patient opretPatient(String cpr, String navn, double vaegt) {
        Patient p = new Patient(cpr, navn, vaegt);
        storage.addPatient(p);
        return p;
    }

    public static Laegemiddel opretLaegemiddel(String navn,
            double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
            double enhedPrKgPrDoegnTung, String enhed) {
        Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet,
                enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung, enhed);
        storage.addLaegemiddel(lm);
        return lm;
    }

    public static void initStorage() {
        opretPatient("121256-0512", "Jane Jensen", 63.4);
        opretPatient("070985-1153", "Finn Madsen", 83.2);
        opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        opretPatient("090149-2529", "Ib Hansen", 87.7);

        opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
                storage.getAllPatienter().get(0), storage.getAllLaegemidler()
                        .get(1),
                123);

        opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
                storage.getAllPatienter().get(0), storage.getAllLaegemidler()
                        .get(0),
                3);

        opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25),
                storage.getAllPatienter().get(3), storage.getAllLaegemidler()
                        .get(2),
                5);

        opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
                storage.getAllPatienter().get(0), storage.getAllLaegemidler()
                        .get(1),
                123);

        opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
                LocalDate.of(2019, 1, 12), storage.getAllPatienter().get(1),
                storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
                LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };

        opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23),
                LocalDate.of(2019, 1, 24), storage.getAllPatienter().get(1),
                storage.getAllLaegemidler().get(2), kl, an);
    }

    //-----------------------------------------------------

    public static List<Patient> getAllPatienter() {
        return storage.getAllPatienter();
    }

    public static List<Laegemiddel> getAllLaegemidler() {
        return storage.getAllLaegemidler();
    }
}
